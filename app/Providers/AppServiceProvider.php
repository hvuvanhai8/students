<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Options;
use App\Models\Socical;
use App\Models\Logo;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);



        view()->composer('web/layout/header',function($view){
            $option = Options::all();
            // dd($option);
            $socical = Socical::all();
            $logo = Logo::first();


            $view->with(['option'=>$option,'socical'=>$socical,'logo'=>$logo]);
        });

        view()->composer('web/layout/footer',function($view){
            $option = Options::all();
            $socical = Socical::all();

            
            $view->with(['option'=>$option,'socical'=>$socical]);
        });

        


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
