<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Options;
use App\Models\Logo;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Models\Solieu;
use App\Models\Banner;
use App\Models\Slide;
use App\Models\Slide_small;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return view('admin.caidat.caidat');
    }
    public function getcaidat()
    {
        //
        $caidat = Options::all();

        
        return view('admin.caidat.caidat', compact('caidat'));
    }
    public function postSua(Request $request)
    {
        $id = $request->id;
        // dd($id);
        $suacaidat = Options::findOrfail($id);
        $suacaidat->value = $request->value;
        $suacaidat->save();

      

        return redirect()->back()->with(['error'=>' Bạn đã thay đổi thành công ']);
    }

    public function getLogo(Request $request)
    {
      $logo = Logo::first();

        return view('admin.logo.logo', compact('logo'));
    }

    public function getAddLogo($id)
    { 
        $logo = Logo::findOrFail($id);
        return view('admin.logo.addlogo', compact('logo'));
    }

    public function postSmLogo(Request $request, $id)
    {
        $logo = Logo::findOrFail($id);
        // $logo->image = $request->name;
        // $logo->link = $request->name;
        if ($request->hasFile('hinh')) {
            $image = $request->file('hinh');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/logo');
            $image->move($destinationPath, $name);
            $logo->image = $name;
    
        }

        $logo->save();
        return redirect()->route('logo');

    }


    public function getSolieu()
    {
        //
        $solieu = Solieu::all();

        
        return view('admin.solieu.solieu', compact('solieu'));
    }

    public function postThemSolieu(Request $request)
    {
        $solieu = new Solieu;
        $solieu->name = $request->name;
        $solieu->percent = $request->percent;
        $solieu->save();

        return redirect()->back()->with(['error'=>' Bạn đã thêm thành công một số liệu mới']);
    }

    public function postSuaSolieu(Request $request)
    {
        $id = $request->id;
        // dd($id);
        $solieu = Solieu::findOrfail($id);
        $solieu->name = $request->name;
        $solieu->percent = $request->percent;
        $solieu->save();

      

        return redirect()->back()->with(['error'=>' Bạn đã thay đổi thành công ']);
    }

    public function getXoaSolieu($id)
    {
        $solieu = Solieu::findOrFail($id);
        $solieu->delete();

        return redirect()->back();
    }



    // banner
    public function getBanner()
    {
        $banner = Banner::paginate(8);

        return view('admin.banner.list', compact('banner'));
    }

    public function getThemBanner()
    {
        return view('admin.banner.add');
    }

    public function postThemBanner(Request $request)
    {
        $banner = new Banner;
        $banner->name = $request->name;
        $banner->title = $request->title;
        $banner->describe_b = $request->editor1;
        $banner->link = $request->link;
        $banner->location = $request->location;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/banner');
            $image->move($destinationPath, $name);
            $banner->image = $name;
    
        }

        $banner->save();

        return redirect()->route('listbanner');
    }

    public function getSuaBanner($id)
    {
        $banner = Banner::findOrFail($id);     

        return view('admin.banner.edit', compact('banner'));
    }

    public function postSuaBanner(Request $request,$id)
    {
        $banner = Banner::findOrFail($id);

        $banner->name = $request->name;
        $banner->title = $request->title;
        $banner->describe_b = $request->editor1;
        $banner->link = $request->link;
        $banner->location = $request->location;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/banner');
            $image->move($destinationPath, $name);
            $banner->image = $name;
    
        }

        $banner->save();
        
        return redirect()->route('listbanner');
    }

    public function getXoaBanner($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->delete();

        return redirect()->back();
    }


    //slide
    public function getSlide()
    {
        $slide = Slide::all();

        return view('admin.slide.list', compact('slide'));
    }

    public function getThemslide()
    {
        return view('admin.slide.add');
    }

    public function postThemSlide(Request $request)
    {
        $slide = new Slide;
        $slide->name = $request->name;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/slide');
            $image->move($destinationPath, $name);
            $slide->image = $name;
    
        }

        $slide->save();

        return redirect()->route('listslide');
    }

    public function getSuaslide($id)
    {
        $slide = Slide::findOrFail($id);     

        return view('admin.slide.edit', compact('slide'));
    }



    public function postSuaslide(Request $request,$id)
    {
        $slide = Slide::findOrFail($id);

        $slide->name = $request->name;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/slide');
            $image->move($destinationPath, $name);
            $slide->image = $name;
    
        }

        $slide->save();

        return redirect()->route('listslide');
    }


    public function getXoaslide($id)
    {
        $slide = Slide::findOrFail($id);
        $slide->delete();

        return redirect()->back();
    }


    // slide small
    public function getSlidesmall()
    {
        $slideS = Slide_small::all();

        return view('admin.slide_small.list', compact('slideS'));
    }

    public function getThemSlidesmall()
    {
        return view('admin.slide_small.add');
    }

    public function postThemSlidesmall(Request $request)
    {
        $slideS = new Slide_small;
        $slideS->name = $request->name;
        $slideS->title = $request->title;
        $slideS->link = $request->link;
        $slideS->location = $request->location;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/slidesmall');
            $image->move($destinationPath, $name);
            $slideS->image = $name;
    
        }

        $slideS->save();

        return redirect()->route('listslidesmall');
    }

    public function getSuaslidesmall($id)
    {
        $slideS = Slide_small::findOrFail($id);     

        return view('admin.slide_small.edit', compact('slideS'));
    }


    public function postSuaslidesmall(Request $request, $id)
    {
        $slideS = Slide_small::findOrFail($id);

        $slideS->name = $request->name;
        $slideS->title = $request->title;
        $slideS->link = $request->link;
        $slideS->location = $request->location;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/slidesmall');
            $image->move($destinationPath, $name);
            $slideS->image = $name;
    
        }

        $slideS->save();

        return redirect()->route('listslidesmall');
    }


    public function getXoaslidesmall($id)
    {
        $slideS = Slide_small::findOrFail($id);
        $slideS->delete();

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
