<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Socical;

class SocicalController extends Controller
{
    //
    public function getSocical()
    {
        $socical = Socical::all();

        return view('admin.socical.list', compact('socical'));
    }

    public function getAddSocical()
    {
        return view('admin.socical.add');
    }

    public function postSubmitSocical(Request $request)
    {
        $socical = new Socical;
        $socical->name = $request->name;
       

        if ($request->hasFile('anhchinh')) {
            $image = $request->file('anhchinh');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/socical');
            $image->move($destinationPath, $name);
            $socical->image = $name;
        }

        if ($request->hasFile('logoto')) {
            $image = $request->file('logoto');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/socical');
            $image->move($destinationPath, $name);
            $socical->image_logo = $name;
    
        }

        if ($request->hasFile('logonho')) {
            $image = $request->file('logonho');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/socical');
            $image->move($destinationPath, $name);
            $socical->image_logo_small = $name;
    
        }

        $socical->link_socical = $request->link;
        $socical->save();
        // return redirect()->back()->with('success','Thêm mạng xã hội thành công!');
        return redirect()->route('listsocical');
    }
    //  lấy view sửa
    public function getEditSocical($id)
    {

        $socical = Socical::findOrFail($id);


        return view('admin.socical.edit', compact('socical'));
    }
    //  xử lý sửa 
    public function postEditSocical(Request $request,$id)
    {
        $socical = Socical::find($id);
        $socical->name = $request->name;
       

        if ($request->hasFile('anhchinh')) {
            $image = $request->file('anhchinh');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/socical');
            $image->move($destinationPath, $name);
            $socical->image = $name;
        }

        if ($request->hasFile('logoto')) {
            $image = $request->file('logoto');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/socical');
            $image->move($destinationPath, $name);
            $socical->image_logo = $name;
    
        }

        if ($request->hasFile('logonho')) {
            $image = $request->file('logonho');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/socical');
            $image->move($destinationPath, $name);
            $socical->image_logo_small = $name;
    
        }

        $socical->link_socical = $request->link;
        $socical->save();

        return redirect()->route('listsocical');
    }


    public function getDelSocical($id)
    {
        $social=Socical::findOrFail($id);
        $social->delete();  

        return redirect()->back();
    }

}
