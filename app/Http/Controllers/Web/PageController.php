<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Options;
use App\Models\Socical;
use App\Models\Solieu;
use App\Models\Banner;
use App\Models\Slide;
use App\Models\Slide_small;

class PageController extends Controller
{
    //
    public function getIndex()
    {

        $solieu = Solieu::select('name','percent','id')->limit('5')->orderBy('id','desc')->get();
        $banner = Banner::all();
        $slide = Slide::all();
        $slideS = Slide_small::all();

        return view('web.page.trangchu', compact('solieu','banner','slide','slideS'));
    }


    public function getAbouts()
    {
        return view('web.page.about');
    }

    public function getdepartments()
    {
        return view('web.page.departments');
    }

    public function getdepartments_details()
    {
        return view('web.page.departments_details');
    }

    public function getliving()
    {
        return view('web.page.living');
    }

    public function getliving_details()
    {
        return view('web.page.living_details');
    }

    public function getnews_even()
    {
        return view('web.page.news_even');
    }

    public function getnews_even_details()
    {
        return view('web.page.news_even_details');
    }

    public function getpartnership()
    {
        return view('web.page.partnership');
    }

    public function getpartnership_details()
    {
        return view('web.page.partnership_details');
    }

    public function getpeople()
    {
        return view('web.page.people');
    }

    public function getpeople_details()
    {
        return view('web.page.people_details');
    }

    public function getprograms()
    {
        return view('web.page.programs');
    }

    public function getstuden_socical()
    {
        $socical = Socical::all();



        return view('web.page.studen_socical', compact('socical'));
    }

    public function getstudent_service_education()
    {
        return view('web.page.student_service_education');
    }

    public function getstudent_service()
    {
        return view('web.page.student_service');
    }

}
