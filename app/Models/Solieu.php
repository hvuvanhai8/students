<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solieu extends Model
{
    //
    protected $table = 'solieu';
    public $timestamps = false;

    protected $fillable = [
        'name', 'percent','describe_solieu','title',
    ];
}
