<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
    //
    protected $table = 'logo';
    public $timestamps = false;

    protected $fillable = [
        'image', 'link',
    ];
}
