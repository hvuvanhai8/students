<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Socical extends Model
{
    //
    public $timestamps = false;
    protected $table = 'socical';

    protected $fillable = [
        'name', 'image', 'image_logo','image_logo_small','link_socical',
    ];
}
