<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide_small extends Model
{
    //
    protected $table = 'slide_small';
    public $timestamps = false;

    protected $fillable = [
        'image', 'name','title','link','location',
    ];
}
