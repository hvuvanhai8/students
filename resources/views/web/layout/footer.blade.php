<?php
// dd($option);

?>


<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12">
                    <div class="about_us_footer">
                        <h3>About us</h3>
                        <p>@foreach($option as $ab) @if($ab->id===112) {{ str_limit($ab->value,200) }} @endif @endforeach</p>

                        <div class="socical_about_us_footer">
                            <ul>
                                @foreach($socical as $so)
                                @if($so ->id !==38)
                                <li><a href="{{ $so->link_socical }}"><img src="../uploadfile/socical/{{ $so->image_logo_small }}" alt="" width="80%"></a></li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-12">
                    <div class="quick_link_footer">
                        <h3>quick links</h3>
                        <ul>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Tuyển sinh Đại học</a></li>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Tuyển sinh Sau Đại học</a></li>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Công tác HSSV</a></li>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Alumni</a></li>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Q&A</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-12">
                    <div class="get_in_touch_footer">
                        <h3>get in touch</h3>
                        <div class="home_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-home color_yellow"></i>	
                                </div>
                                <div class="col-md-11">
                                    <div class="content_right_home_get_in_touch_footer">
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 1:</strong>@foreach($option as $phone1) @if($phone1->id===103) {{ $phone1->value }} @endif @endforeach</span>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 2:</strong>@foreach($option as $phone1) @if($phone1->id===104) {{ $phone1->value }} @endif @endforeach</span>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 3:</strong>
                                                        @foreach($option as $phone1)
                                                       
                                                            @if($phone1->id===105)
                                                            
                                                            {{ $phone1->value }} 

                                                            @endif 
                                                        @endforeach</span>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="phone_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-phone-volume color_yellow"></i>
                                </div>
                                <div class="col-md-11">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="phone_left_phone_get_in_touch_footer">
                                                <p>@foreach($option as $phone1) @if($phone1->id===106) {{ $phone1->value }} @endif @endforeach</p>
                                                <p>@foreach($option as $phone1) @if($phone1->id===107) {{ $phone1->value }} @endif @endforeach</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="phone_right_phone_get_in_touch_footer">
                                                <p><strong class="color_yellow">Đại học: </strong>@foreach($option as $phone1) @if($phone1->id===108) {{ $phone1->value }} @endif @endforeach | @foreach($option as $phone1) @if($phone1->id===109) {{ $phone1->value }} @endif @endforeach</p>
                                                <p><strong class="color_yellow">Sau đại học: </strong>@foreach($option as $phone1) @if($phone1->id===110) {{ $phone1->value }} @endif @endforeach</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="mail_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-envelope color_yellow"></i>
                                </div>
                                <div class="col-md-11">
                                        <span>@foreach($option as $phone1) @if($phone1->id===111) {{ $phone1->value }} @endif @endforeach</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>