	<!-- header -->
	<header>
		<div class="top_header">
			<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-12">
							<div class="container">
									<div class="main_header_content">
										<div class="left_content_header">
											<p class="color_yellow">Liên hệ hỗ trợ :</p>
											<p><i class="fas fa-phone-volume color_yellow"></i><a href="#" class="color_white">@foreach($option as $phone1) @if($phone1->id===100) {{ $phone1->value }} @endif @endforeach</a><span class="color_yellow"> | </span><a href="#" class="color_white">@foreach($option as $phone1) @if($phone1->id===101) {{ $phone1->value }} @endif @endforeach</a></p>
											<p class="color_white"><i class="far fa-envelope color_yellow"></i>@foreach($option as $phone1) @if($phone1->id===102) {{ $phone1->value }} @endif @endforeach</p>
										</div>
										<div class="login_header">
											<a href="#" class="color_blue">Đăng ký</a>
											<span class="color_blue">hoặc</span>
											<a href="#"class="color_blue">Đăng nhập</a>
											<p class="color_yellow"><a href="#" class="color_yellow">Sử dụng Facebook</a></p>
										</div>
									</div>
							</div>
							
							<div class="language_header">
								<a href="#" class="color_yellow">Tiếng Việt</a>
								<a href="#" class="color_white">English</a>
							</div>
						</div>
					</div>
			</div>
		</div>
		

	<!-- menu chính -->
		<div class="menu_h">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12 col-12">
							<div class="logo_h">
								<a href="/index">
									<img src="../uploadfile/logo/{{ $logo->image }}" alt="" width="75%">
								</a>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 col-12">
							<div class="main_menu">
								<ul>
									<li><a href="#">Nhà đất bán</a><span class="color_yellow"> | </span></li>
									<li><a href="#">Nhà đất cho thuê</a><span class="color_yellow"> | </span></li>
									<li><a href="#">Dự án</a><span class="color_yellow"> | </span></li>
									<li><a href="#">Cần mua cần thuê</a><span class="color_yellow"> | </span></li>
									<li><a href="#">Tin tức</a><span class="color_yellow"></span></li>
									<li><a href="#">Xây dựng - kiến trúc</a><span class="color_yellow"> | </span></li>
									<li><a href="#">Nội - ngoại thất</a><span class="color_yellow"> | </span></li>
									<li><a href="#">Phong thủy</a><span class="color_yellow"> | </span></li>
									<li><a href="#">Danh bạ</a><span class="color_yellow"></span></li>
								</ul>
							</div>
							<form action="" class="form_search"> 
									<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Nhập từ khóa tìm kiếm">
											<div class="input-group-append">
											<button class="btn btn-success" type="submit">Tìm kiếm</button>  
											</div>
									</div>
							</form>
						</div>
					</div>
				</div>
		</div>
	<!-- end menu chính -->

	<!-- menu nhỏ bên phải -->
		<!-- <div class="menu_right">
				<div class="tool_icon_right tool_show_txt" style="z-index: 3;">
				<span class="trigger"></span>
				<div class="inner">
					<p class="item item_border item_a">
						<a class="toolAction" href=""><i class="fab fa-instagram"></i></a>
					</p>
					<p class="item item_border item_b">
						<a class="toolAction" href="#"><img src="images/home/subiz.png" alt="" width="70%"></a>
					</p>
					<p class="item item_border item_a">
						<a class="toolAction" href="#"><i class="fab fa-facebook-f"></i></a>
					</p>
					<p class="item item_border item_b">
						<a href="#" target="_blank"><img src="images/home/youtube.png" alt="" width="60%"></a>
					</p>
					<p class="item item_border item_a">
						<a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
					</p>
					<p class="item item_border item_b">
						<a class="toolAction" href="#"><img src="images/home/zalo.png" alt="" width="60%"></a>
					</p>
					<p class="item item_border item_a">
						<a href="#" target="_blank"><i class="far fa-envelope"></i></a>
					</p>
					<p class="item item_border item_b">
						<a href="#" target="_blank"><img src="images/home/printer.svg" alt="" width="50%"></a>
					</p>
					<p class="item get_info item_a">
						<a href="#" target="_blank">nhận thông tin từ chúng tôi</a>
					</p>
				</div>
				</div>
		</div> -->
	<!-- end menu nhỏ bên phải -->

	</header>
	<!-- end header -->