@extends('web.layout.master')

@section('content')
<main>
    <div id="departments">
        <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_gray">DEPARTMENTS</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                    <img src="http://giaiphapdaotaovnnp.edu.vn/images/chuy%E1%BB%87n-sinh-vi%C3%AAn.jpg" alt="" width="100%">
                            </div>
                            <div class="row txt_ct_cate">
                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                    <img src="images/squares.svg" alt="" width="60%">
                                </div>
                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <a href="#"><h5>Department of Foudation</h5></a>
                                        <hr>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                    <img src="http://giaiphapdaotaovnnp.edu.vn/images/chuy%E1%BB%87n-sinh-vi%C3%AAn.jpg" alt="" width="100%">
                            </div>
                            <div class="row txt_ct_cate">
                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                    <img src="images/squares.svg" alt="" width="60%">
                                </div>
                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <a href="#"><h5>Department of Social Sciences, Economics and Manangement</h5></a>
                                        <hr>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                    <img src="http://giaiphapdaotaovnnp.edu.vn/images/chuy%E1%BB%87n-sinh-vi%C3%AAn.jpg" alt="" width="100%">
                            </div>
                            <div class="row txt_ct_cate">
                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                    <img src="images/squares.svg" alt="" width="60%">
                                </div>
                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <a href="#"><h5>Department of Natural Sciences and Technology</h5></a>
                                        <hr>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection