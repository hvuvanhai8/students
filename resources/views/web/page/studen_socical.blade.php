@extends('web.layout.master')

@section('content')

<main>
    <div id="social">
        <div class="social_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_gray">VNU-isoical</span>
                        </div>
                    </div>
                </div>
        </div>
        
        <div class="main_soial">
            <div class="container">
                <div class="row">
                    @foreach($socical as $so)
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_main_h">
                            <a href="{{ $so->link_socical }}">
                                <div class="img_content_main_h">
                                    <img src="../uploadfile/socical/{{ $so->image }}" alt="" width="100%" height="350">
                                    <div class="mark_img_social">
                                    </div>
                                    <div class="img_icon_social">
                                        <img src="../uploadfile/socical/{{ $so->image_logo }}" alt="" width="50%">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</main>

@endsection