@extends('web.layout.master')

@section('content')

<main>
    <div id="people_details">
        <div class="people_details_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_blue">PEOPLE > </span><span class="color_gray">Office of Personnel and Administrative affairs</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people_details">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_left_pp_details">
                            <div class="cate_pp_details">
                                <h3>Categories</h3>
                                <ul>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="60%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Office of Personnel	and Administrative affairs</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Office of Research and Partnership development</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Office of Academic Affairs</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Center for Quality Assurance and Testing</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Center for Globalization, Research, Consulting and Career Orientation service</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Office of Planning and Finance</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Office of Student Affairs</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Staff Directory</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Lectuers Directory</a>
                                                </div>
                                            </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="dif_info">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Các thông tin khác</h5>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <ul>
                                                <li>
                                                    <div class="row">
                                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                <i class="fas fa-angle-double-right"></i>
                                                            </div>
                                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                <a href="#">Thông báo - Tin tức</a>
                                                            </div>
                                                    </div>
                                                </li>
                                                <li>
                                                        <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="#">Thời khóa biểu - Lịch thi</a>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li>
                                                        <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="#">Mẫu đơn - Xác nhận</a>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li>
                                                        <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="#">Học bổng</a>
                                                                </div>
                                                        </div>
                                                </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="insta_pp">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="tag_pp">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                </div>
                                <div class="row a_tag_content">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-12">
                        <div class="content_right_pp_details">
                                <div class="row tt_right_pp_details">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="23%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h3 class="color_blue">Office of Personnel and Administrative affairs</h3>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                            <p>To succeed in this era of a knowledge economy and global integration, will and enthusiasm are not enough. Learners should be equipped with scientific knowledge, standard professional skills, and proficiency in foreign languages for international integration, with the ability to work effectively on a global scale. A training institution which produces highly qualified human resources must be able to equip learners with the capacity to use those basic qualities after they graduate. Vietnam National University, Hanoi - International School(VNU- IS) is one of the higher education institutions that has been entrusted with the mission of imparting such values and has been conducting it successfully.
                                                    With 15 years of construction and development, and as one of the first institutions to offer undergraduate and graduate programs totally in foreign languages, VNU-IS continues to bring into full play its experience to seize opportunities, overcome difficulties and challenges, and gradually carry out a fundamental and comprehensive renovation in line with Party and State policies in order to delevelop into a major autonomous training and research center, a pioneer in the cause of VNU’s renovation and international integration; step-by-step it has increased the scope and improved quality, especially of training and integration.
                                                    Over the past years, VNU-IS has created and built up basic foundations for its development into a training institution with a unique and highly professional international academic environment, including linkages and cooperation with prestigious foreign institutions. VNU-IS has built up a highly qualified contingent of Vietnamese and foreign lecturers with a great deal of expertise and professionalism from different countries such as USA, Australia, Taiwan, Malaysia, Hongkong, Sweden to meet teaching and training requirements for all its advanced joint international programs that are conducted totally in foreign languages. All its graduates with degrees awarded by VNU and foreign institutions have been greatly appreciated for the training quality by all their employing organizations.
                                                    In 2016 and the following years, together with the whole country and under VNU’s clear-sighted and effective leaderhip, VNU-IS will bring into full play its pioneer status and role as the first autonomous institution in the country in line with Government’s Decree No. 16, dated 14 February, 2015; it will accelerate innovation activities; </p>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection