@extends('web.layout.master')

@section('content')

<main>
    <div id="partnership">
        <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_gray">PARTNERSHIP</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                    <img src="https://i2.wp.com/www.edutimes.vn/wp-content/uploads/2018/07/sinh-vien-thich-den-cac-nuoc-chau-A.jpg?resize=744%2C405&ssl=1" alt="" width="100%">
                            </div>
                            <div class="row txt_ct_cate">
                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                    <img src="images/squares.svg" alt="" width="30%">
                                </div>
                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                        <hr>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                            </div>
                            
                        </div>
                        <div class="cate_pp_img">
                                <div class="img_cate">
                                        <img src="https://i2.wp.com/www.edutimes.vn/wp-content/uploads/2018/07/sinh-vien-thich-den-cac-nuoc-chau-A.jpg?resize=744%2C405&ssl=1" alt="" width="100%">
                                </div>
                                <div class="row txt_ct_cate">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="images/squares.svg" alt="" width="30%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                    </div>
                                </div>
                                
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                            <div class="cate_pp_img">
                                <div class="img_cate">
                                        <img src="https://i2.wp.com/www.edutimes.vn/wp-content/uploads/2018/07/sinh-vien-thich-den-cac-nuoc-chau-A.jpg?resize=744%2C405&ssl=1" alt="" width="100%">
                                </div>
                                <div class="row txt_ct_cate">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="images/squares.svg" alt="" width="30%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="cate_pp_img">
                                    <div class="img_cate">
                                            <img src="https://i2.wp.com/www.edutimes.vn/wp-content/uploads/2018/07/sinh-vien-thich-den-cac-nuoc-chau-A.jpg?resize=744%2C405&ssl=1" alt="" width="100%">
                                    </div>
                                    <div class="row txt_ct_cate">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="30%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </div>
                                    </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection