@extends('web.layout.master')

@section('content')

<main>
    <div id="student_service">
        <div class="student_service_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_blue">STUDENT SERVICE > </span><span class="color_blue">STUDENT SERVICE'LIFE ></span><span class="color_blue">student Clubs > </span><span class="color_blue">IShuffle > </span><span class="color_gray">IShuffle in Hanoi Music Festival</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people_details">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_left_pp_details">
                            <div class="cate_pp_details">
                                <h3>MORE</h3>
                                <ul>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="35%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">IShuffle make fun in Hanoi Music Festival</a>
                                                    <span>June 28, 2018</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="35%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">IShuffle make fun in Hanoi Music Festival</a>
                                                    <span>June 28, 2018</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="35%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">IShuffle make fun in Hanoi Music Festival</a>
                                                    <span>June 28, 2018</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="35%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">IShuffle make fun in Hanoi Music Festival</a>
                                                    <span>June 28, 2018</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="35%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">IShuffle make fun in Hanoi Music Festival</a>
                                                    <span>June 28, 2018</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="35%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">IShuffle make fun in Hanoi Music Festival</a>
                                                    <span>June 28, 2018</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="img_student_service">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="img_student_service">
                                            <img src="images/student_service/student.jpg" alt="" width="100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="video_youtube_student_service">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="content_video_youtube_student_service">
                                            <iframe width="100%" height="auto" src="https://www.youtube.com/embed/snNFMlFmyzg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div> 
                                        <div class="txt_content_video_youtube">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 col-8">
                                                    <span>Rihanna - Don't Stop The Music</span>
                                                    <span>3:54</span>
                                                    <span>IShuffle</span>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                    <img src="images/student_service/youtube.svg" alt="" width="80%">
                                                </div>
                                            </div>
                                        </div>         
                                    </div>
                                </div>
                            </div>
                            <div class="insta_pp">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="50%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="tag_pp">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="50%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                </div>
                                <div class="row a_tag_content">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-12">
                        <div class="content_right_pp_details">
                                <div class="row tt_right_pp_details">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="13%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h3 class="color_blue">Office of Personnel and Administrative affairs</h3>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                            <p>To succeed in this era of a knowledge economy and global integration, will and enthusiasm are not enough. Learners should be equipped with scientific knowledge, standard professional skills, and proficiency in foreign languages for international integration, with the ability to work effectively on a global scale. A training institution which produces highly qualified human resources must be able to equip learners with the capacity to use those basic qualities after they graduate. Vietnam National University, Hanoi - International School(VNU- IS) is one of the higher education institutions that has been entrusted with the mission of imparting such values and has been conducting it successfully.
                                                    With 15 years of construction and development, and as one of the first institutions to offer undergraduate and graduate programs totally in foreign languages, VNU-IS continues to bring into full play its experience to seize opportunities, overcome difficulties and challenges, and gradually carry out a fundamental and comprehensive renovation in line with Party and State policies in order to delevelop into a major autonomous training and research center, a pioneer in the cause of VNU’s renovation and international integration; step-by-step it has increased the scope and improved quality, especially of training and integration.
                                                    Over the past years, VNU-IS has created and built up basic foundations for its development into a training institution with a unique and highly professional international academic environment, including linkages and cooperation with prestigious foreign institutions. VNU-IS has built up a highly qualified contingent of Vietnamese and foreign lecturers with a great deal of expertise and professionalism from different countries such as USA, Australia, Taiwan, Malaysia, Hongkong, Sweden to meet teaching and training requirements for all its advanced joint international programs that are conducted totally in foreign languages. All its graduates with degrees awarded by VNU and foreign institutions have been greatly appreciated for the training quality by all their employing organizations.
                                                    In 2016 and the following years, together with the whole country and under VNU’s clear-sighted and effective leaderhip, VNU-IS will bring into full play its pioneer status and role as the first autonomous institution in the country in line with Government’s Decree No. 16, dated 14 February, 2015; it will accelerate innovation activities; 
                                                    o succeed in this era of a knowledge economy and global integration, will and enthusiasm are not enough. Learners should be equipped with scientific knowledge, standard professional skills, and proficiency in foreign languages for international integration, with the ability to work effectively on a global scale. A training institution which produces highly qualified human resources must be able to equip learners with the capacity to use those basic qualities after they graduate. Vietnam National University, Hanoi - International School(VNU- IS) is one of the higher education institutions that has been entrusted with the mission of imparting such values and has been conducting it successfully.
                                                    With 15 years of construction and development, and as one of the first institutions to offer undergraduate and graduate programs totally in foreign languages, VNU-IS continues to bring into full play its experience to seize opportunities, overcome difficulties and challenges, and gradually carry out a fundamental and  </p>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="related_news_serivce">
                                            <h5>related news</h5>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="13%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Hue’s Imperial City shines bright in new promo video</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="13%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Hue lists French architectural heritages for conservation</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="13%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">When in Hue, take a tour in the old Pagoda of the Celestial Lady</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection