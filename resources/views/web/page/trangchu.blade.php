@extends('web.layout.master')
@section('content')
<main>
    <div id=home>
        <div class="banner_h">
            <div class="img_banner_h">
                <div class="slide_banner">
                    <div class="owl-carousel owl-theme slide_img_banner">
                        @foreach($slide as $sl)
                        <div class="item">
                            <img src="../uploadfile/slide/{{ $sl->image }}" alt="" width="100%" height="">
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- <div class="text_img">
                    <p class="top_text">Tìm kiếm ngôi nhà mà bạn yêu thích</p>
                    <p class="down_text">CÙNG BẠN TÌM NHÀ VÀ XÂY TỔ ẤM</p>
                </div> -->
                <div class="mark_opacity">

                </div>
                <div class="mark_banner">
                    <div class="container">
                        <div class="row">
                           
                          
                            
                                @foreach($banner as $bn)
                                    @if($bn->location===0)
                                    <div class="col-md-4 col-sm-12 col-12">
                                        <div class="img_mark_banner">
                                            <div class="img_hh">
                                                <a href="{{ $bn->link }}"> <img src="../uploadfile/banner/{{ $bn->image }}" alt="" width="60%"></a> 
                                            </div>
                                        </div>
                                        <div class="txt_mark_banner">
                                            <h3>{{ $bn->title }}</h3>
                                            <p>{!! str_limit($bn->describe_b,180) !!}</p>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="news_home">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12">
                        <h2>Dự Án Mới</h2>
                        <p>dự án mới đã mở bán từ ngày 21/2/2018</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="test_color"></div>

        <div class="slide_home">
            <div class="container">
                <div class="owl-carousel owl-theme slide_img_home">
                    @foreach($slideS as $sls)
                        @if($sls->location===0)
                            <div class="item">
                                <a href="{{ $sls->link }}"> <img src="../uploadfile/slidesmall/{{ $sls->image }}" alt="" width="80%" height="200px"></a>
                                <div class="title_img_slide_home"><h6>{{ $sls->title }}</h6></div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="color_under_slide"></div>

        <div class="activities_home">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12">
                        <h2>Activities</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                    </div>
                </div>
                <div class="row h1_r">
                        <div class="mark_banner">
                                <div class="container">
                                    <div class="row">
                                        <?php $sttt=1; ?>
                                        @foreach($banner as $bn)
                                            @if($bn->location===1)
                                                @if($sttt<=6)
                                                <div class="col-md-4 col-sm-12 col-12">
                                                    <div class="img_mark_banner">
                                                        <div class="img_hh">
                                                            <a href="{{ $bn->link }}"><img src="../uploadfile/banner/{{ $bn->image }}" alt="" width="50%"></a>
                                                        </div>
                                                    </div>
                                                    <div class="txt_mark_banner">
                                                        <h3>{{ $bn->title }}</h3>
                                                        <p>{!! str_limit($bn->describe_b,120) !!}</p>
                                                    </div>
                                                </div>
                                                @endif 
                                                <?php $sttt++; ?>
                                            @endif
                                       
                                        @endforeach
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
        </div>

        <div class="color_under_activities"></div>

        <div class="Facts_Figures_home">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12">
                        <h2>Facts & Figures</h2>
                        <p>Số liệu về kết quả và thành tựu của sinh viên - học viên Khoa Quốc tế sau khi tốt nghiệp</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12">
                        <div class="load_percent">
                            <div class="flex-wrapper">

                                @foreach($solieu as $sl)
                                <div class="single-chart">
                                    <svg viewBox="0 0 36 36" class="circular-chart orange">
                                    <path class="circle-bg"
                                        d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                    />
                                    <path class="circle"
                                        stroke-dasharray="{{ $sl->percent }}, 100"
                                        d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                    />
                                    <text x="18" y="20.35" class="percentage">{{ $sl->percent }}%</text>
                                    </svg>
                                    <div class="txt_percent">
                                    <p>{{ $sl->name }}</p>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>

        <div class="color_under_Facts_Figures_home"></div>

        <div class="slide_home_bottom">
            <div class="container">
                <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <h2>Alumni</h2>
                            <p>The success stories of the VNU-IS alumni </p>
                        </div>
                </div>
            </div>
            <div class="container">
                <div class="owl-carousel owl-theme slide_img_home">
                    @foreach($slideS as $sls)
                        @if($sls->location===1)
                            <div class="item">
                                <a href="{{ $sls->link }}"> <img src="../uploadfile/slidesmall/{{ $sls->image }}" alt="" width="80%" height="200px"></a>
                                <div class="title_img_slide_home"><h6>{{ $sls->title }}</h6></div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="form_call_book">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-12">
                        <div class="call_to_action">
                            <form action="">
                                <div class="row">
                                    <div class="col-md-8 col-sm-12 col-12">
                                        <h3>call to action</h3>
                                        <div class="input_group_call_to_action">
                                            <input type="text" class="form-control" id="usr" name="username" placeholder="name">
                                            <input type="mail" class="form-control" id="usr" name="mail" placeholder="email">
                                            <input type="text" class="form-control" id="usr" name="phone" placeholder="phone">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="p_4_action">Thông tin của bạn chỉ được chúng tôi sử dụng cho việc gửi thông tin từ Khoa Quốc tế tới bạn và không được tiết lộ cho bên thứ ba.</p>
                                        <button type="submit" class="btn btn-warning" value="Submit"><strong>brochure</strong><span> download</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="book_library">
                            <h3>book library</h3>
                            <strong class="color_blue">Using student ID</strong>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            <a href="#" class="color_blue">https://www.library.is.vnu.edu.vn</a>
                            <div class="button_book_library">
                                    <button type="button" class="btn btn-warning" value="Submit"><strong>find more</strong></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</main>
@endsection