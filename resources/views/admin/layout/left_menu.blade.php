<ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Danh Mục</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> Danh sách danh mục</a></li>
                <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Thêm danh mục</a></li>
                <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Sửa danh mục</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Bài viết</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> Danh sách bài viết</a></li>
                <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Thêm bài viết</a></li>
                <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Sửa bài viết</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Cài đặt</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('caidat') }}"><i class="fa fa-circle-o"></i> Quản lý thông tin chung</a></li>
                <li><a href="{{ route('logo') }}"><i class="fa fa-circle-o"></i> Quản lý logo</a></li>
                <li><a href="{{ route('listsocical') }}"><i class="fa fa-circle-o"></i> Quản lý mạng xã hội</a></li>
                <li><a href="{{ route('listSolieu') }}"><i class="fa fa-circle-o"></i> Quản lý số liệu % trang chủ</a></li>
                <li><a href="{{ route('listbanner') }}"><i class="fa fa-circle-o"></i> Quản banner trang chủ</a></li>
                <li><a href="{{ route('listslide') }}"><i class="fa fa-circle-o"></i> Quản Slide trang chủ</a></li>
                <li><a href="{{ route('listslidesmall') }}"><i class="fa fa-circle-o"></i> Quản Slide nhỏ</a></li>
            </ul>
        </li>

      
        
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>