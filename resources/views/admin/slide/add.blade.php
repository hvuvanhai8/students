@extends('admin.layout.master')

@section('content')


<div class="container">
        <form action="{{ route('Themslide') }}" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>Tên slide </label>
                    <input class="form-control" name="name" placeholder="Tên ảnh slide..." required/>
                </div>
                <div class="form-group">
                    <label>Ảnh slide hiển thị</label>
                    <input type="file" name="image" required>
                </div>
            
                
                <button type="submit" class="btn btn-default">Thêm mới</button>
                <button type="reset" class="btn btn-default">Làm mới</button>
            <form>
</div>


@endsection