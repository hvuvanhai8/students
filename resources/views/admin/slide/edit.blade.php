@extends('admin.layout.master')

@section('content')


<div class="container">
        <form action="{{ asset('admin/edit-slide')}}/{{ $slide->id }}" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>Tên mạng xã hội </label>
                    <input class="form-control" name="name" placeholder="Tên slide cũ..." value="{{ $slide->name }}"/>
                </div>
                <div class="form-group">
                    <label>Ảnh mạng xã hội</label>
                    <input type="file" name="image" >
                    <img src="uploadfile/slide/{{ $slide->image }}" alt="" width="120" height="70">
                </div>
            
                
                <button type="submit" class="btn btn-default">Sửa</button>
                <button type="reset" class="btn btn-default">Làm mới</button>
            <form>
</div>


@endsection