@extends('admin.layout.master')

@section('content')


  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h2 class="box-title">QUẢN SLIDE TRANG CHỦ</h2><br/><br/>
          <a href="{{ route('addslide') }}" class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>
          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th class="col-md-1">STT</th>
              <th class="col-md-1">Tên slide</th>
              <th class="col-md-2">Ảnh slide</th>
              <th class="col-md-2">Hành động</th>
            </tr>
            <?php $stt=1;?>
            @foreach($slide as $sl)
            <tr>
              <td>{{$stt++ }}</td>
              <td>{{ $sl->name }}</td>
              <td><img src="uploadfile/slide/{{ $sl->image }}" alt="" width="190" height="90"></td>
              <td>
                    <a href="{{asset('admin/edit-slide')}}/{{ $sl->id }}" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                    <a href="" class="btn btn-danger delitem" id="" data-toggle="modal" data-target="#modal-default-xoa-{{ $sl->id }}"><i class="fa fa-trash"></i></a>
                    {{-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger">
                        Xóa
                      </button> --}}
            </td>
            </tr>
            {{-- modal --}}
            <div class="modal fade" id="modal-default-xoa-{{ $sl->id }}" style="display: none;">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                      <h4 class="modal-title">Bạn có chắc chắn muốn xóa hình ảnh slide này?</h4>
                    </div>
                    <div class="modal-body">
                      {{-- <p>One fine body…</p> --}}
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <a href="{{ route('Xoaslide', $sl->id) }}" class="btn btn-danger delitem" title="">Xóa</a>
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
            @endforeach
          </tbody></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>



</div>







  @endsection