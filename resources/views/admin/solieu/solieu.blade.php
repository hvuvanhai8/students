@extends('admin.layout.master')

@section('content')
    

<div class="container">
    <h2>Quản lý thông tin chung website</h2>  
    <h3>Quản lý số liệu phần trăm</h3>
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-default-add">
        Thêm
       </button>
    @if (session('error'))
        <div class="alert alert-success an_thongbao">
                <strong>{{ session('error') }}!</strong>
        </div>
    @endif         
    <table class="table">
      <thead>
        <tr>
          <th class="col-md-2">STT</th>
          <th class="col-md-2">Tên số liệu</th>
          <th class="col-md-3">Tỷ lệ phần trăm</th>
          <th class="col-md-3">Hành động</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $stt=1;
          ?>
            @foreach($solieu as $sl)
            <tr>
                <td>{{ $stt++ }}</td>
                <td>{{ $sl->name }}</td>
                <td>{{ $sl->percent }} %</td>
                <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default-{{ $sl->id }}">
                       Sửa
                      </button>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default-xoa-{{ $sl->id }}">
                        Xóa
                       </button>
                      {{-- <a href="{{asset('admin/xoa-solieu')}}/{{ $sl->id }}" class="btn btn-danger delitem" id=""><i class="fa fa-trash"></i></a> --}}
                      <div class="modal fade" id="modal-default-{{ $sl->id }}" style="display: none;">
                            <form action="{{ route('SuaSolieu') }}" method="post">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title">Thông tin cần thay đổi</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                                {{-- <div>
                                                    <strong>giá trị cũ: </strong><span>{{ $cd->value }}</span>
                                                </div> --}}
                                                <input type="hidden" name="id" value="{{ $sl->id }}">
                                                <input type="text" class="form-control" id="usr" name="name" value="{{ $sl->name }}" placeholder="nhập giá trị mới"><br/>
                                                <input type="text" class="form-control" id="usr" name="percent" value="{{ $sl->percent }}" placeholder="nhập giá trị mới">
                                               
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Lưu lại</button>
                                    </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                {{ csrf_field() }}
                            </form>
                            <!-- /.modal-dialog -->
                    </div>
                    {{-- modal xoa --}}
                    <div class="modal fade" id="modal-default-xoa-{{ $sl->id }}" style="display: none;">
                        <form action="" method="post">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Bạn có chắc chắn muốn xóa số liệu này</h4>
                                </div>
                                <div class="modal-body">
                                    
                                            {{-- <div>
                                                <strong>giá trị cũ: </strong><span>{{ $cd->value }}</span>
                                            </div> --}}
                                            <input type="hidden" name="id" value="{{ $sl->id }}">
                                            <input type="hidden" class="form-control" id="usr" name="name" value="{{ $sl->name }}" placeholder="nhập giá trị mới">
                                            <input type="hidden" class="form-control" id="usr" name="percent" value="{{ $sl->percent }}" placeholder="nhập giá trị mới">
                                           
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                                    <a href="{{asset('admin/xoa-solieu')}}/{{ $sl->id }}" class="btn btn-danger delitem" id=""><i class="fa fa-trash"></i></a>
                                </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            {{ csrf_field() }}
                        </form>
                        <!-- /.modal-dialog -->
                    </div>
                </td>
            </tr>
            @endforeach
            
      </tbody>
    </table>
</div>
<div class="modal fade" id="modal-default-add" style="display: none;">
    <form action="{{ route('ThemSolieu') }}" method="post">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Thêm số liệu mới</h4>
            </div>
            <div class="modal-body">
                
                        {{-- <div>
                            <strong>giá trị cũ: </strong><span>{{ $cd->value }}</span>
                        </div> --}}
                        <input type="hidden" name="id" value="">
                        <input type="text" class="form-control" id="usr" name="name" value="" placeholder="Tên số liệu">
                        <input type="text" class="form-control" id="usr" name="percent" value="" placeholder="Phần trăm số liệu mới">
                       
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Lưu lại</button>
            </div>
            </div>
            <!-- /.modal-content -->
        </div>
        {{ csrf_field() }}
    </form>
    <!-- /.modal-dialog -->
</div>   
    
<script>
    $(document).ready(function(){
				$('.an_thongbao').delay(5000).slideUp();
				
			});
    alert('hai');
</script>


@endsection

@section('script')


@endsection