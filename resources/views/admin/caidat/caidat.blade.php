@extends('admin.layout.master')

@section('content')
    

<div class="container">
    <h2>Quản lý thông tin chung website</h2>  
    @if (session('error'))
        <div class="alert alert-success an_thongbao">
                <strong>{{ session('error') }}!</strong>
        </div>
    @endif         
    <table class="table">
      <thead>
        <tr>
          <th class="col-md-2">Tên thông tin</th>
          <th class="col-md-2">Vị trí</th>
          <th class="col-md-3">giá trị</th>
          <th class="col-md-2">Ảnh mô tả</th>
          <th class="col-md-3">action</th>
        </tr>
      </thead>
      <tbody>
            @foreach($caidat as $cd)
            <tr>
                <td>{{ $cd->description }}</td>
                <td>{{ $cd->position }}</td>
                <td>{{ $cd->value }}</td>
                <td>
                    
                </td>
                <td><button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default-{{ $cd->id }}">
                       Sửa
                      </button>
                    
                      <div class="modal fade" id="modal-default-{{ $cd->id }}" style="display: none;">
                            <form action="{{ route('suacaidat') }}" method="post">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title">Thông tin cần thay đổi</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                                {{-- <div>
                                                    <strong>giá trị cũ: </strong><span>{{ $cd->value }}</span>
                                                </div> --}}
                                                <input type="hidden" name="id" value="{{ $cd->id }}">
                                                <input type="text" class="form-control" id="usr" name="value" value="{{ $cd->value }}" placeholder="nhập giá trị mới">
                                               
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Lưu lại</button>
                                    </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                {{ csrf_field() }}
                            </form>
                            <!-- /.modal-dialog -->
                    </div>
                    
                </td>
            </tr>
            @endforeach
      </tbody>
    </table>
</div>
   
    
<script>
    $(document).ready(function(){
				$('.an_thongbao').delay(5000).slideUp();
				
			});
    alert('hai');
</script>


@endsection

@section('script')


@endsection