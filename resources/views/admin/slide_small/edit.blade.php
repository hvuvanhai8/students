@extends('admin.layout.master')

@section('content')


<div class="container">
    <div class="row">
            <form action="{{ route('Suaslidesmall', $slideS->id) }}" enctype="multipart/form-data" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label>Tên</label>
                        <input class="form-control" name="name" placeholder="Tên..." value="{{ $slideS->name }}" >
                    </div>
                    
                    <input type="radio" name="location" value="0" {{ ($slideS->location==0) ? "checked" : "" }} required> Trong tin tức<br>

                    <input type="radio" name="location" value="1" {{ ($slideS->location==1) ? "checked" : "" }} required> Trong sinh viên<br> 

                    <div class="form-group">
                        <label>Ảnh cũ</label>
                        <br/>
                        <img src="uploadfile/slidesmall/{{ $slideS->image }}" alt="" width="190" height="90">
                        <input type="file" name="image" >
                    </div>
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input class="form-control" name="title" placeholder="Tiêu đề..." value="{{ $slideS->title }}" >
                    </div>
                    <div class="form-group">
                        <label>Link </label>
                        <input class="form-control" name="link" placeholder="link..." value="{{ $slideS->link }}" >
                    </div>
                
                    
                    <button type="submit" class="btn btn-default">Sửa</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                <form>
    </div>
       
</div>


@endsection

@section('style')

<style>
    .container{
        width: 1000px;
    }
</style>
    
@endsection

@section('script')

<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>

<script src="https://cdn.ckeditor.com/[version.number]/[distribution]/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'editor1' );
</script>

@endsection