@extends('admin.layout.master')

@section('content')


  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h2 class="box-title">QUẢN LÝ MẠNG XÃ HỘI</h2><br/><br/>
          <a href="{{ route('addsocical') }}" class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>
          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th class="col-md-1">STT</th>
              <th class="col-md-1">Tên mạng xã hội</th>
              <th class="col-md-2">Ảnh mạng xã hội</th>
              <th class="col-md-2">logo to</th>
              <th class="col-md-2">logo nhỏ</th>
              <th class="col-md-2">link</th>
              <th class="col-md-2">Hành động</th>
            </tr>
            <?php $stt=1;?>
            @foreach($socical as $so)
            <tr>
              <td>{{$stt++ }}</td>
              <td>{{ $so->name }}</td>
              <td><img src="uploadfile/socical/{{ $so->image }}" alt="" width="120" height="70"></td>
              <td><img src="uploadfile/socical/{{ $so->image_logo }}" alt="" width="70" height="70"></td>
              <td><img src="uploadfile/socical/{{ $so->image_logo_small }}" alt="" width="25" height="25"></td>
              <td>{{ $so->link_socical }}</td>
              <td>
                    <a href="{{asset('admin/sua-socical')}}/{{ $so->id }}" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                    <a href="" class="btn btn-danger delitem" id="" data-toggle="modal" data-target="#modal-default-xoa-{{ $so->id }}"><i class="fa fa-trash"></i></a>
                    {{-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger">
                        Xóa
                      </button> --}}
            </td>
            </tr>
            {{-- modal --}}
            <div class="modal fade" id="modal-default-xoa-{{ $so->id }}" style="display: none;">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                      <h4 class="modal-title">Bạn có chắc chắn muốn xóa mạng xã hội này?</h4>
                    </div>
                    <div class="modal-body">
                      {{-- <p>One fine body…</p> --}}
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <a href="{{asset('admin/xoa-socical')}}/{{ $so->id }}" class="btn btn-danger delitem" title="">Xóa</a>
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
            @endforeach
          </tbody></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>



</div>







  @endsection