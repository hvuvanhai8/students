@extends('admin.layout.master')

@section('content')


<div class="container">
        <form action="{{ route('Submitsocical') }}" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>Tên mạng xã hội </label>
                    <input class="form-control" name="name" placeholder="Tên mạng xã hội..." required/>
                </div>
                <div class="form-group">
                    <label>Ảnh mạng xã hội</label>
                    <input type="file" name="anhchinh" required>
                </div>
                <div class="form-group">
                    <label>Logo to</label>
                    <input type="file" name="logoto" required>
                </div>
                <div class="form-group">
                    <label>Logo nhỏ</label>
                    <input type="file" name="logonho">
                </div>
                <div class="form-group">
                    <label>link </label>
                    <input class="form-control" name="link" placeholder="link mạng xã hội..." required/>
                </div>
            
                
                <button type="submit" class="btn btn-default">Thêm mới</button>
                <button type="reset" class="btn btn-default">Làm mới</button>
            <form>
</div>


@endsection