@extends('admin.layout.master')

@section('content')


<div class="container">
        <form action="{{ asset('admin/sua-socical')}}/{{ $socical->id }}" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>Tên mạng xã hội </label>
                    <input class="form-control" name="name" placeholder="Tên mạng xã hội..." value="{{ $socical->name }}"/>
                </div>
                <div class="form-group">
                    <label>Ảnh mạng xã hội</label>
                    <input type="file" name="anhchinh" >
                    <img src="uploadfile/socical/{{ $socical->image }}" alt="" width="120" height="70">
                </div>
                <div class="form-group">
                    <label>Logo to</label>
                    <input type="file" name="logoto" >
                    <img src="uploadfile/socical/{{ $socical->image_logo }}" alt="" width="70" height="70">
                </div>
                <div class="form-group">
                    <label>Logo nhỏ</label>
                    <input type="file" name="logonho">
                    <img src="uploadfile/socical/{{ $socical->image_logo_small }}" alt="" width="25" height="25">
                </div>
                <div class="form-group">
                    <label>link </label>
                    <input class="form-control" name="link" placeholder="link mạng xã hội..." value="{{ $socical->link_socical }}" />
                </div>
            
                
                <button type="submit" class="btn btn-default">Sửa</button>
                <button type="reset" class="btn btn-default">Làm mới</button>
            <form>
</div>


@endsection