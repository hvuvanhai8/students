@extends('admin.layout.master')

@section('content')


<div class="container">
        <form action="{{ route('Thembanner') }}" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>Tên</label>
                    <input class="form-control" name="name" placeholder="Tên..." required/>
                </div>
                <input type="radio" name="location" value="0"  required> Trong slide<br>
                <input type="radio" name="location" value="1" checked required> Trong hoạt động<br>
                <div class="form-group">
                    <label>Ảnh</label>
                    <input type="file" name="image" required>
                </div>
                <div class="form-group">
                    <label>Tiêu đề</label>
                    <input class="form-control" name="title" placeholder="Tiêu đề..." required/>
                </div>
                <div class="form-group">
                    <label>Mô tả</label>
                    <textarea name="editor1"></textarea>
                </div>
                <div class="form-group">
                    <label>Link </label>
                    <input class="form-control" name="link" placeholder="link..." required/>
                </div>
            
                
                <button type="submit" class="btn btn-default">Thêm mới</button>
                <button type="reset" class="btn btn-default">Làm mới</button>
            <form>
</div>


@endsection

@section('style')

<style>
    .container{
        width: 1000px;
    }
</style>
    
@endsection



@section('script')

<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>

<script src="https://cdn.ckeditor.com/[version.number]/[distribution]/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'editor1' );
</script>

@endsection