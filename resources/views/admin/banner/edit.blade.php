@extends('admin.layout.master')

@section('content')


<div class="container">
    <div class="row">
            <form action="{{ asset('admin/edit-banner')}}/{{ $banner->id }}" enctype="multipart/form-data" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label>Tên</label>
                        <input class="form-control" name="name" placeholder="Tên..." value="{{ $banner->name }}" >
                    </div>
                    
                    <input type="radio" name="location" value="0" {{ ($banner->location==0) ? "checked" : "" }} required> Trong slide<br>

                    <input type="radio" name="location" value="1" {{ ($banner->location==1) ? "checked" : "" }} required> Trong hoạt động<br> 

                    <div class="form-group">
                        <label>Ảnh cũ</label>
                        <br/>
                        <img src="uploadfile/banner/{{ $banner->image }}" alt="" width="70" height="70">
                        <input type="file" name="image" >
                    </div>
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input class="form-control" name="title" placeholder="Tiêu đề..." value="{{ $banner->title }}" >
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea name="editor1">{{ $banner->describe_b }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Link </label>
                        <input class="form-control" name="link" placeholder="link..." value="{{ $banner->link }}" >
                    </div>
                
                    
                    <button type="submit" class="btn btn-default">Sửa</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                <form>
    </div>
       
</div>


@endsection

@section('style')

<style>
    .container{
        width: 1000px;
    }
</style>
    
@endsection

@section('script')

<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>

<script src="https://cdn.ckeditor.com/[version.number]/[distribution]/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'editor1' );
</script>

@endsection