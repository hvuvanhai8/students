<?php 

use Illuminate\Support\Facades\Route;


Route::get('/login', 'Admin\LoginController@getLogin')->name('admin.login');

// login admin
Route::post('/postlogin', 'Admin\LoginController@postLogin')->name('admin.post.login');


Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {
    Route::get('/', function () {
        return view('welcome');
    });
    
    Route::get('/ad-min', function () {
        return view('admin.index');
    });
    
    Auth::routes();
    
    Route::get('/home', 'HomeController@index')->name('home');
    // caidat
    Route::get('/caidat', 'Admin\OptionController@getcaidat')->name('caidat');
    Route::post('/caidat-sua', 'Admin\OptionController@postSua')->name('suacaidat');
    // logo
    Route::get('/logo', 'Admin\OptionController@getLogo')->name('logo');
    Route::get('/Addlogo/{id}', 'Admin\OptionController@getAddLogo')->name('addlogo');
    Route::post('/submit-logo/{id}', 'Admin\OptionController@postSmLogo')->name('Smlogo');
    //socical
    Route::get('/socical', 'Admin\SocicalController@getSocical')->name('listsocical');
    Route::get('/addsocical', 'Admin\SocicalController@getAddSocical')->name('addsocical');
    Route::post('/add-socical', 'Admin\SocicalController@postSubmitSocical')->name('Submitsocical');
    Route::get('/xoa-socical/{id}', 'Admin\SocicalController@getDelSocical')->name('delsocical');
    Route::get('/sua-socical/{id}', 'Admin\SocicalController@getEditSocical')->name('editsocical');
    Route::post('/sua-socical/{id}', 'Admin\SocicalController@postEditSocical')->name('Suasocical');
    // số lieu % trang chu
    Route::get('/solieu', 'Admin\OptionController@getSolieu')->name('listSolieu');
    Route::post('/them-solieu', 'Admin\OptionController@postThemSolieu')->name('ThemSolieu');
    Route::post('/sua-solieu', 'Admin\OptionController@postSuaSolieu')->name('SuaSolieu');
    Route::get('/xoa-solieu/{id}', 'Admin\OptionController@getXoaSolieu')->name('XoaSolieu');
    // banner
    Route::get('/banner', 'Admin\OptionController@getBanner')->name('listbanner');
    Route::get('/add-banner', 'Admin\OptionController@getThemBanner')->name('addbanner');
    Route::post('/add-banner', 'Admin\OptionController@postThemBanner')->name('Thembanner');
    Route::get('/edit-banner/{id}', 'Admin\OptionController@getSuaBanner')->name('editbanner');
    Route::post('/edit-banner/{id}', 'Admin\OptionController@postSuaBanner')->name('Suabanner');
    Route::get('/xoa-baner/{id}', 'Admin\OptionController@getXoaBanner')->name('Xoabanner');
    // slide
    Route::get('/slide', 'Admin\OptionController@getSlide')->name('listslide');
    Route::get('/add-slide', 'Admin\OptionController@getThemSlide')->name('addslide');
    Route::post('/add-slide', 'Admin\OptionController@postThemSlide')->name('Themslide');
    Route::get('/edit-slide/{id}', 'Admin\OptionController@getSuaslide')->name('editslide');
    Route::post('/edit-slide/{id}', 'Admin\OptionController@postSuaslide')->name('Suaslide');
    Route::get('/xoa-slide/{id}', 'Admin\OptionController@getXoaslide')->name('Xoaslide');
    // slide small 
    Route::get('/slidesmall', 'Admin\OptionController@getSlidesmall')->name('listslidesmall');
    Route::get('/add-slidesmall', 'Admin\OptionController@getThemSlidesmall')->name('addslidesmall');
    Route::post('/add-slidesmall', 'Admin\OptionController@postThemSlidesmall')->name('Themslidesmall');
    Route::get('/edit-slidesmall/{id}', 'Admin\OptionController@getSuaslidesmall')->name('editslidesmall');
    Route::post('/edit-slidesmall/{id}', 'Admin\OptionController@postSuaslidesmall')->name('Suaslidesmall');
    Route::get('/xoa-slidesmall/{id}', 'Admin\OptionController@getXoaslidesmall')->name('Xoaslidesmall');

// logout admin
    Route::get('/getlogout', 'Admin\LoginController@getLogout')->name('admin.logout');
});