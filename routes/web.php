<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

// frontend
Route::get('/', 'Web\PageController@getIndex')->name('home');

Route::get('/abouts', 'Web\PageController@getAbouts')->name('abouts');

Route::get('/departments', 'Web\PageController@getdepartments')->name('departments');

Route::get('/departments_details', 'Web\PageController@getdepartments_details')->name('departments_details');

Route::get('/living', 'Web\PageController@getliving')->name('living');

Route::get('/living_details', 'Web\PageController@getliving_details')->name('living_details');

Route::get('/news_even', 'Web\PageController@getnews_even')->name('news_even');

Route::get('/news_even_details', 'Web\PageController@getnews_even_details')->name('news_even_details');

Route::get('/partnership', 'Web\PageController@getpartnership')->name('partnership');

Route::get('/partnership_details', 'Web\PageController@getpartnership_details')->name('partnership_details');

Route::get('/people', 'Web\PageController@getpeople')->name('people');

Route::get('/people_details', 'Web\PageController@getpeople_details')->name('people_details');

Route::get('/programs', 'Web\PageController@getprograms')->name('programs');

Route::get('/student_socical', 'Web\PageController@getstuden_socical')->name('studen_socical');

Route::get('/student_service_education', 'Web\PageController@getstudent_service_education')->name('student_service_education');

Route::get('/student_service', 'Web\PageController@getstudent_service')->name('student_service');

require_once('admin.php');